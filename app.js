let balanceElement = document.getElementById("balance");
let loanElement = document.getElementById("loan");
let loanAmountElement = document.getElementById("loan-amount");
let repayElement = document.getElementById("repay");
let salaryElement = document.getElementById("salary");
let depositElement = document.getElementById("deposit");
let workForSalaryElement = document.getElementById("work-for-salary");
let laptopsElement = document.getElementById("laptops");
let descElement = document.getElementById("desc");
let pictureElement = document.getElementById("picture");
let titleElement = document.getElementById("title");
let featuresElement = document.getElementById("features");
let priceElement = document.getElementById("price");
let payElement = document.getElementById("pay");


let salary = 0.0;
let balance = 0.0;
let loanAmount = 0.0;


// Fetches laptop objects from the API and sends the data to addLaptopsToSelection
// for further processing. Runs update money to show correct balances.
const init = () => {
    fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
        .then(response => response.json())
        .then(data => laptops = data)
        .then(laptops => addLaptopsToSelection(laptops));

    updateBalances();
}

// Goes through each laptop object and sends them to the 
// function addLaptopToSelection
// Shows data and image for the first laptop
const addLaptopsToSelection = (laptops) => {
    laptops.forEach(x => addLaptopToSelection(x));
    priceElement.innerText = laptops[0].price;
    descElement.innerText = laptops[0].description;
    titleElement.innerText = laptops[0].title;
    handleFeatures(laptops[0]);
    handleImage(laptops[0]);
};

// Adds each laptop object to the select element as an option.
const addLaptopToSelection = (laptop) => {
    const laptopElement = document.createElement("option");
    laptopElement.value = laptop.id;
    laptopElement.appendChild(document.createTextNode(laptop.title));
    laptopsElement.appendChild(laptopElement);
};

// Changes laptop to the current laptop selected
// while updating the info and image.
const handleLaptopSelectChange = e => {
    const selectedLaptop = laptops[e.target.selectedIndex];
    priceElement.innerText = selectedLaptop.price;
    descElement.innerText = selectedLaptop.description;
    titleElement.innerText = selectedLaptop.title;
    handleFeatures(selectedLaptop);
    handleImage(selectedLaptop);
};

// Creates the image tag, sends url and image element for fetching data.
// Appends image element to the correct div.
const handleImage = (selectedLaptop) => {
    pictureElement.innerText = "";
    let imageElement = new Image();
    const url = `https://noroff-komputer-store-api.herokuapp.com/${selectedLaptop.image}`
    fetchImage(url, imageElement);
    pictureElement.appendChild(imageElement);
};

// Fetches image from url and stores the images as a blob before
// before attaching a image object URL to the image element.
// Tries again with png filetype if jpg returned 404.
const fetchImage = (url, imageEl) => {
    fetch(url)
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not OK');
            }
            return response.blob();
        })
        .then(imageBlob => {
            const imageObjectURL = URL.createObjectURL(imageBlob);
            imageEl.src = imageObjectURL;
        })
        .catch(error => {
            console.error('There has been a problem with your fetch operation:', error);
            if (url.match(".jpg")) {
                fetchImage(url.replace(/.jpg$/, ".png"), imageEl)
            }
        })
}

// Clears the feature element for previous specs and
// calling handleFeature for each spec of current laptop
const handleFeatures = (selectedLaptop) => {
    featuresElement.innerText = "";
    selectedLaptop.specs.forEach(x => handleFeature(x));
};


// Creates a p tag for appending a spec from the array of specs
const handleFeature = (feature) => {
    const featureElement = document.createElement("p");
    featureElement.className = "card-text";
    featureElement.id = "feature";
    featureElement.appendChild(document.createTextNode(feature));
    featuresElement.appendChild(featureElement);
};

// Switch case for handling each transaction type
// If loanAmount is below 0, meaning the customer has money to spare
// it will be transfered to the balance account
const transaction = (money, setting) => {
    switch (setting) {
        case "balance":
            balance += money;
            break;
        case "salary":
            salary += money;
            break;
        case "loan":
            loanAmount += money;
            if (loanAmount < 0) {
                transaction(loanAmount * -1, "balance");
                loanAmount = 0;
            }
            toggleRepay()
            break;
    }
    updateBalances();
}

// Checks if balance is equal to or more than the price for the selected laptop
// shows an appropriate alert after the check.
const handlePay = () => {
    const selectedLaptop = laptops[laptopsElement.selectedIndex];
    if (balance >= selectedLaptop.price && selectedLaptop.stock > 0) {
        transaction(selectedLaptop.price * -1, "balance");
        selectedLaptop.stock -= 1;
        alert(`Congratulations! You are now the owner of the ${selectedLaptop.title}.`);
    } else {
        alert(`Unfortunately you cannot afford the ${selectedLaptop.title}.`);
    }
}

// Prompts the user to enter a loan amount, if user enters a value that is more than
// double the balance it will go into a while loop prompting for a different value.
// Sends the request to the transaction function for both loan and balance.
const handleLoan = () => {
    if (loanAmount === 0) {
        let loanRequest = prompt("Enter a loan amount.");
        while (loanRequest > 2 * balance) {
            loanRequest = prompt("You cannot loan more than double of your bank balance. Enter a new loan amount.");
        };
        if (!isNaN(loanRequest) && loanRequest >= 0) {
            transaction(parseFloat(loanRequest), "loan");
            transaction(parseFloat(loanRequest), "balance");
        }
    } else {
        alert("You may not have two loans at once. Pay the initial loan first.");
    }
};

// Sends the full salary amount to the loan transaction as a negative number
// and resets the salary value.
const handleRepay = () => {
    const moneyRepaid = salary
    salary = 0;
    transaction(moneyRepaid * -1, "loan");
};

// Deposits salary to balance, if there is a loan
// it will send 10% to pay off loan.
const handleDeposit = () => {
    const moneyDeposited = salary
    salary = 0;
    if (loanAmount > 0) {
        transaction(moneyDeposited * -0.1, "loan");
        transaction(moneyDeposited * 0.9, "balance");
    } else { 
        transaction(moneyDeposited, "balance"); 
    }
};

// Increases salary by 100
const handleSalary = () => {
    transaction(100, "salary");
};

// Toggles the repay button if there is a loan amount
const toggleRepay = () => {
    if (loanAmount > 0) {
        repayElement.className = "btn btn-danger visible"
    } else {
        repayElement.className = "btn btn-danger invisible"
    }
};

// Updates all balances in NOK currency format
const updateBalances = () => {
    loanAmountElement.innerText = new Intl.NumberFormat('no-NO', { style: 'currency', currency: 'NOK' })
        .format(loanAmount);
    salaryElement.innerText = new Intl.NumberFormat('no-NO', { style: 'currency', currency: 'NOK' })
        .format(salary);
    balanceElement.innerText = new Intl.NumberFormat('no-NO', { style: 'currency', currency: 'NOK' })
        .format(balance);
};

// Event listeners for the different functions of the page
laptopsElement.addEventListener("change", handleLaptopSelectChange);
payElement.addEventListener("click", handlePay);
loanElement.addEventListener("click", handleLoan);
repayElement.addEventListener("click", handleRepay);
depositElement.addEventListener("click", handleDeposit);
workForSalaryElement.addEventListener("click", handleSalary);

init();